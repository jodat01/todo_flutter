import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_01/classes/todoScreen.dart';
import 'package:http/http.dart' as http;

class fetchdatascreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return fetchdatascreenState();
  }
}

class fetchdatascreenState extends State<fetchdatascreen> {
  late Future<List<TODO>> _futureTODO;

  @override
  void initState() {
    super.initState();
    _futureTODO = fetchtodos();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Get todos'),
      ),
      body: Center(
        child: FutureBuilder<List<TODO>>(
          future: _futureTODO,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<TODO>? data = snapshot.data;
              return ListView.builder(
                  itemCount: data!.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                        height: 75,
                        color: Colors.white,
                        child: Column(
                          children: <Widget>[
                            Text('User id: ' + data[index].id),
                            Text('Task:' + data[index].title),
                          ],
                        ));
                  });
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            // By default show a loading spinner.
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}

class TODO {
  final String id;
  final String title;
  //final bool completed;

  TODO({required this.id, required this.title});

  factory TODO.fromJson(Map<String, dynamic> json) {
    return TODO(
      id: json['userId'],
      title: json['title'],
      //completed: json['completed'],
    );
  }
}

Future<List<TODO>> fetchtodos() async {
  final response = await http.get(Uri.parse('http://localhost:3000/TODO'));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    List jsonResponse = json.decode(response.body);
    return jsonResponse.map((data) => new TODO.fromJson(data)).toList();
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load todos');
  }
}
