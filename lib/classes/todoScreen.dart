import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';

class todoscreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return todoscreenState();
  }
}

class todoscreenState extends State<todoscreen> {
  final TextEditingController userid_controller = TextEditingController();
  final TextEditingController title_controller = TextEditingController();
  Future<List<TODO>>? _futureTODO;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ToDos'),
      ),
      body: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(8.0),
        child: (_futureTODO == null)
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextField(
                    controller: userid_controller,
                    decoration: InputDecoration(hintText: 'Enter User id'),
                  ),
                  TextField(
                    controller: title_controller,
                    decoration: InputDecoration(hintText: 'Enter Task title'),
                  ),
                  ElevatedButton(
                    child: Text('Create Data'),
                    onPressed: () {
                      setState(() {
                        _futureTODO = create_todo(
                            userid_controller.text, title_controller.text);
                        //_futureTODO = create_todo(title_controller.text);
                      });
                    },
                  ),
                ],
              )
            : FutureBuilder<List<TODO>>(
                future: _futureTODO,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<TODO>? data = snapshot.data;
                    return ListView.builder(
                        itemCount: data!.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                              height: 75,
                              color: Colors.white,
                              child: Column(
                                children: <Widget>[
                                  Text('User id: ' + data[index].id),
                                  Text('Task:' + data[index].title),
                                ],
                              ));
                        });
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }

                  return CircularProgressIndicator();
                },
              ),
      ),
    );
  }
}

class TODO {
  final String id;
  final String title;
  //final bool completed;

  TODO({required this.id, required this.title});

  factory TODO.fromJson(Map<String, dynamic> json) {
    return TODO(
      id: json['userId'],
      title: json['title'],
      //completed: json['completed'],
    );
  }
}

Future<List<TODO>> create_todo(String title, id) async {
  final response = await http.post(
    Uri.parse('http://localhost:3000/TODO'),
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode({
      'userId': id,
      'title': title,
    }),
  );

  if (response.statusCode == 201) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    List jsonResponse = json.decode(response.body);
    return jsonResponse.map((data) => new TODO.fromJson(data)).toList();
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to create Todo.');
  }
}
