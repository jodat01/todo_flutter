import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app_01/classes/todoScreen.dart';
import 'package:flutter_app_01/classes/fetchDataScreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TODO App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: userlogin(title: 'TODO login page'),
    );
  }
}

String userid = '007';
String pwd = '12345';

const userId = const {
  '007': '12345',
  '101': 'hunter',
};

class userlogin extends StatefulWidget {
  final onLogin;
  final String title;

  userlogin({required this.title, this.onLogin});

  @override
  State<StatefulWidget> createState() {
    return loginState();
  }
}

class loginState extends State<userlogin> {
  final TextEditingController userIdController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('ToDO Login'),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              TextButton(
                  child: Text(
                    'Fetch Data',
                    style: TextStyle(fontSize: 30),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => fetchdatascreen()),
                    );
                  }),
              TextButton(
                  child: Text(
                    'Create Data',
                    style: TextStyle(fontSize: 30),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => todoscreen()),
                    );
                  }),
              TextButton(
                  child: Text(
                    'Delete Data',
                    style: TextStyle(fontSize: 30),
                  ),
                  onPressed: () {}),
              TextButton(
                  child: Text(
                    'Update Data',
                    style: TextStyle(fontSize: 30),
                  ),
                  onPressed: () {}),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
          ),
        ));
  }
}
